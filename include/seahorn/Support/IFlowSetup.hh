/*
 * Setup the information flow analysis. 
 *
 * We will collect information about what variables are 
 * expected to be secret, what are the conditions under
 * which information flow is allowed, etc. in this class
 * and then query it in other classes.
 *
 */

#ifndef __INFORMATION_FLOW_SETUP_H_DEFINED__
#define __INFORMATION_FLOW_SETUP_H_DEFINED__

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Analysis/MemoryBuiltins.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/ADT/BitVector.h"
#include "boost/unordered_set.hpp"
#include "llvm/Support/Debug.h"
#include "avy/AvyDebug.h"
#include <assert.h>

namespace seahorn
{
  using namespace llvm;
  
  class IFlowSetup : public llvm::ModulePass
  {
  public:
  // types.
    struct SecretGlobalInfo {
      GlobalVariable* GV;
      GlobalVariable *GV1, *GV2;
      Function* InitF;
    };

    struct PublicGlobalInfo {
      GlobalVariable* GV;
      GlobalVariable *GV1, *GV2;
      Function *EQF;
    };

  // member variables.
    std::vector<GlobalVariable*> secretGlobals;
    std::vector<GlobalVariable*> publicGlobals;
    std::vector< SecretGlobalInfo > secretGlobalInfo;
    std::vector< PublicGlobalInfo > publicGlobalInfo;
    std::map<GlobalVariable*, Function*> eqCmpFnMap;
    std::set<Function*> noCopyFns;

    static char ID;

    IFlowSetup () : 
        llvm::ModulePass (ID)
        { 
          // avy::AvyEnableLog ("ifsetup");
        }

    virtual bool runOnModule (llvm::Module &M);
    virtual bool runOnFunction (Function &F);

    virtual void getAnalysisUsage (llvm::AnalysisUsage &AU) const;
    virtual const char* getPassName () const {return "InformationFlowAnalysisSetup";}
    void setupGlobals(llvm::Module& M);

    friend class ComparisonFnFinder;
  };

  struct SecureGlobalFinder : public InstVisitor<SecureGlobalFinder>
  {
    std::map<GlobalVariable*, Function*> secretGlobals;

    // visit a call instruction.
    void visitCallInst (CallInst& I) { CallSite CS(&I); visitCallSite(CS, I); };
    // visit an invoke instruction.
    void visitInvokeInst (InvokeInst& I) { CallSite CS(&I); visitCallSite(CS, I); };
    // This one does the actual work.
    void visitCallSite(CallSite& CS, Instruction& I);
  };

  class ComparisonFnFinder : public InstVisitor<ComparisonFnFinder>
  {
    // members.
    IFlowSetup& parent;
    // methods.
    void visitCallSite(CallSite& CS, Instruction& I);
  public:
    // constructor.
    ComparisonFnFinder(IFlowSetup& p) : parent(p) {}
    // visitor methods.
    void visitCallInst (CallInst& I) { CallSite CS(&I); visitCallSite(CS, I); };
    void visitInvokeInst (InvokeInst& I) { CallSite CS(&I); visitCallSite(CS, I); };
  };
}
#endif//__INFORMATION_FLOW_SETUP_H_DEFINED__
