#pragma once

#include "seahorn/Analysis/CanAccessMemory.hh"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Analysis/MemoryBuiltins.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/ADT/BitVector.h"
#include "boost/unordered_set.hpp"
#include "llvm/Support/Debug.h"
#include "avy/AvyDebug.h"
#include <assert.h>
#include "seahorn/Transforms/Instrumentation/IFlowAnalysis.hh"

namespace seahorn
{
  using namespace llvm;
  
  class InsertSecurityAnnotations : public llvm::ModulePass
  {
  protected:
  public:
    static char ID;

  // methods.
  public:

    InsertSecurityAnnotations () : 
        llvm::ModulePass (ID)
        { 
          avy::AvyEnableLog ("iftypes");
        }
    
    virtual bool runOnModule (llvm::Module &M);
    virtual bool runOnFunction (Function &F);

    virtual void getAnalysisUsage (llvm::AnalysisUsage &AU) const;
    virtual const char* getPassName () const {return "InsertSecurityAnnotations";}
  };

}

