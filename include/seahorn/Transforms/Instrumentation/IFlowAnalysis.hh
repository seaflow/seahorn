#ifndef __INFORMATION_FLOW_ANALYSIS_HH__
#define __INFORMATION_FLOW_ANALYSIS_HH__

#include "seahorn/Analysis/CanAccessMemory.hh"
#include "seahorn/Support/IFlowSetup.hh"

#include "llvm/Pass.h"
#include "llvm/IR/Module.h"
#include "llvm/IR/IRBuilder.h"
#include "llvm/IR/DataLayout.h"
#include "llvm/Analysis/MemoryBuiltins.h"
#include "llvm/Target/TargetLibraryInfo.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/ADT/BitVector.h"
#include "boost/unordered_set.hpp"
#include "llvm/Support/Debug.h"
#include "avy/AvyDebug.h"
#include <assert.h>

namespace seahorn
{
  using namespace llvm;
  
  class IFlowAnalysis : public llvm::ModulePass
  {
  public:
  // types.
    typedef IFlowSetup::SecretGlobalInfo SecretGlobalInfo;
    typedef IFlowSetup::PublicGlobalInfo PublicGlobalInfo;

  protected:
  //members
    ValueToValueMapTy globalP1map, globalP2map;
    std::vector<Function*> functionsToCopy;

  public:
    static char ID;

  // methods.
  public:

    IFlowAnalysis () : 
        llvm::ModulePass (ID)
        { 
          // avy::AvyEnableLog ("ifsetup");
        }
    
    virtual bool runOnModule (llvm::Module &M);
    virtual bool runOnFunction (Function &F);

    virtual void getAnalysisUsage (llvm::AnalysisUsage &AU) const;
    virtual const char* getPassName () const {return "InformationFlowAnalysis";}
  private:
    void createNewGlobals(llvm::Module& M, IFlowSetup& ifsetup);
    void createNewFunctions(llvm::Module& M, IFlowSetup& ifsetup);
    void deleteOldGlobals(llvm::Module& M, IFlowSetup& ifsetup);
    void deleteOldFunctions(llvm::Module& M, IFlowSetup& ifsetup);
    void insertNewMain(llvm::Module& M, IFlowSetup& ifsetup);
    llvm::Function* insertVerifierError(llvm::Module& M);
    llvm::Function* insertVerifierAssert(llvm::Module& M, llvm::Function* func_error);
    void assertEquality(llvm::Module& M, llvm::BasicBlock* bb, llvm::Function* func_assert, PublicGlobalInfo& pgi);
    void insertInit(llvm::Module& M, llvm::BasicBlock* bb, SecretGlobalInfo& sgi);

    llvm::GlobalVariable* cloneGlobal(llvm::Module& M, GlobalVariable* GV, const char* suffix, ValueToValueMapTy& map);
    void createNewFunction(llvm::Module& M, llvm::Function* F);

    // static functions.
    static void setNoUnwindAttribute(llvm::Function* func, llvm::Module& M, bool noReturn = false);
    static void setNoUnwindAttribute(llvm::CallInst* callinst, llvm::Module& M);

    friend class EqualityComparisonFinder;
  };

  struct KeepaliveDeleter : public InstVisitor<KeepaliveDeleter>
  {
    void visitCallInst (CallInst& I) { CallSite CS(&I); visitCallSite(CS, I); };
    void visitInvokeInst (InvokeInst& I) { CallSite CS(&I); visitCallSite(CS, I); };
    void visitCallSite(CallSite& CS, Instruction& I);
  };

}

#endif
