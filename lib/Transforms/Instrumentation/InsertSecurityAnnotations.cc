/* 
   Instrument a program with "security types."
*/

#include "seahorn/Transforms/Instrumentation/InsertSecurityAnnotations.hh"

#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Transforms/Utils/UnifyFunctionExitNodes.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/CallSite.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/CommandLine.h"

#include <boost/optional.hpp>

#include "avy/AvyDebug.h"

#include <iostream>
//#include "seahorn/Analysis/Steensgaard.hh"

namespace seahorn
{
  using namespace llvm;

  char IFlowAnalysis::ID = 0;

  bool IFlowAnalysis::runOnModule (llvm::Module &M)
  {
    (errs() << "InsertSecurityAnnotations::runOnModule >>").write_escaped(M.getName());
    errs() << "\n";
    return false;
  }


  bool IFlowAnalysis::runOnFunction (Function &F)
  {
    (errs() << "InsertSecurityAnnotations::runOnFunction >>").write_escaped(F.getName());
    errs() << "\n";
    return false;
  }
    
  void IFlowAnalysis::getAnalysisUsage (llvm::AnalysisUsage &AU) const
  {
    //AU.setPreservesAll ();
    //AU.addRequiredTransitive<llvm::SteensgaardDataStructures> ();
  } 

}

static llvm::RegisterPass<seahorn::IFlowAnalysis> 
X ("InsertSecurityAnnotations", "Insert annotations that correspond to the \"security types\" of each variable.");
   

