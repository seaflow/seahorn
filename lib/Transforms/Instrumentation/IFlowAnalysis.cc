/* 
   Add annotations corresponding to security types.
*/

#include "seahorn/Transforms/Instrumentation/IFlowAnalysis.hh"

#include "llvm/Analysis/AliasAnalysis.h"
#include "llvm/Transforms/Utils/UnifyFunctionExitNodes.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"
#include "llvm/Transforms/Utils/Cloning.h"
#include "llvm/ADT/SmallVector.h"
#include "llvm/IR/InstIterator.h"
#include "llvm/IR/CallSite.h"
#include "llvm/Support/Debug.h"
#include "llvm/Support/CommandLine.h"

#include <boost/optional.hpp>

#include "avy/AvyDebug.h"

#include <iostream>
//#include "seahorn/Analysis/Steensgaard.hh"

namespace seahorn
{
  using namespace llvm;

  char IFlowAnalysis::ID = 0;

  bool IFlowAnalysis::runOnModule (llvm::Module &M)
  {
    // Kill the calls to keep alive.
    KeepaliveDeleter KaD;
    KaD.visit(M);

    IFlowSetup& ifsetup = getAnalysis<IFlowSetup>();

    createNewGlobals(M, ifsetup);
    createNewFunctions(M, ifsetup);
    deleteOldFunctions(M, ifsetup);
    deleteOldGlobals(M, ifsetup);
    insertNewMain(M, ifsetup);
    LOG("ifsetup", M.dump());

    return true;
  }

  llvm::GlobalVariable* IFlowAnalysis::cloneGlobal(llvm::Module& M, GlobalVariable* GV, const char* suffix, ValueToValueMapTy& map)
  {
      PointerType* GVPtrType = GV->getType();
      Type* GVType = GVPtrType->getElementType();
      GlobalVariable* GVp = new GlobalVariable(
                              M, GVType, 
                              GV->isConstant(),
                              GV->getLinkage(), 
                              GV->getInitializer(), 
                              GV->getName() + suffix, 
                              GV, GV->getThreadLocalMode(), 
                              GVPtrType->getAddressSpace(),
                              GV->isExternallyInitialized());
      map[GV] = GVp;
      return GVp;
  }

  void IFlowAnalysis::createNewGlobals(llvm::Module& M, IFlowSetup& ifsetup)
  {
    LOG("ifsetup", errs() << "creating new globals.\n");
    for (int i=0; i < ifsetup.secretGlobals.size(); i++) {
      GlobalVariable* GV = ifsetup.secretGlobals[i];
      ifsetup.secretGlobalInfo[i].GV1 = cloneGlobal(M, GV, ".p1", globalP1map);
      ifsetup.secretGlobalInfo[i].GV2 = cloneGlobal(M, GV, ".p2", globalP2map);
      ifsetup.noCopyFns.insert(ifsetup.secretGlobalInfo[i].InitF);
    }
    for (auto GV : ifsetup.publicGlobals) {
      auto GV1 = cloneGlobal(M, GV, ".p1", globalP1map);
      auto GV2 = cloneGlobal(M, GV, ".p2", globalP2map);
      auto pos = ifsetup.eqCmpFnMap.find(GV);
      Function* EQF = pos != ifsetup.eqCmpFnMap.end() ? pos->second : NULL;

      LOG("ifsetup", (errs() << "GV: ").write_escaped(GV->getName()) << "; ");
      LOG("ifsetup", (errs() << "GV1: ").write_escaped(GV1->getName()) << "; ");
      LOG("ifsetup", (errs() << "GV2: ").write_escaped(GV2->getName()) << "; ");
      LOG("ifsetup", (errs() << "EQF: "));
      LOG("ifsetup", (EQF ? errs().write_escaped(EQF->getName()) : (errs() << "NULL")) << "\n");

      PublicGlobalInfo pgi = { GV, GV1, GV2, EQF };
      ifsetup.publicGlobalInfo.push_back(pgi);
    }
  }

  void IFlowAnalysis::deleteOldGlobals(llvm::Module& M, IFlowSetup& ifsetup)
  {
    for (auto GV : ifsetup.secretGlobals) {
      GV->eraseFromParent();
    }
    for (auto GV : ifsetup.publicGlobals) {
      GV->eraseFromParent();
    }
  }

  void IFlowAnalysis::createNewFunctions(llvm::Module& M, IFlowSetup& ifsetup)
  {
    // first grab the functions to copy.
    LOG("ifsetup", errs() << "identifying functions to copy.\n");
    for (Function &F : M) {
      if (!F.empty() && ifsetup.noCopyFns.find(&F) == ifsetup.noCopyFns.end()) {
        Function *Fp1 = Function::Create(F.getFunctionType(), F.getLinkage(), F.getName() + ".p1", &M);
        Function *Fp2 = Function::Create(F.getFunctionType(), F.getLinkage(), F.getName() + ".p2", &M);

        globalP1map[&F] = Fp1;
        globalP2map[&F] = Fp2;

        for (auto A = F.arg_begin(), AE = F.arg_end(), Ap1 = Fp1->arg_begin(), Ap2 = Fp2->arg_begin(); 
                  A != AE; A++, Ap1++, Ap2++) {
          Ap1->takeName(A);
          Ap2->takeName(A);
          globalP1map[A] = Ap1;
          globalP2map[A] = Ap2;
        }

        functionsToCopy.push_back(&F);

        LOG("ifsetup", errs() << "will copy ");
        LOG("ifsetup", errs().write_escaped(F.getName()) << " to ");
        LOG("ifsetup", errs().write_escaped(Fp1->getName()) << " and ");
        LOG("ifsetup", errs().write_escaped(Fp2->getName()) << "\n");
      }
    }

    // and now copy them.
    for (Function *F : functionsToCopy) {
      LOG("ifsetup", (errs() << "copying: ").write_escaped(F->getName()) << "\n");
      createNewFunction(M, F);
    }
  }

  void IFlowAnalysis::deleteOldFunctions(llvm::Module& M, IFlowSetup& ifsetup)
  {
    for (auto F : functionsToCopy) {
      F->eraseFromParent();
    }
  }

  void IFlowAnalysis::createNewFunction(llvm::Module& M, llvm::Function* F)
  {
    SmallVector<ReturnInst*, 4> R1, R2;
    ValueToValueMapTy emptyMap;

    auto pos1 = globalP1map.find(F);
    auto pos2 = globalP2map.find(F);

    assert(pos1 != globalP1map.end() && isa<Function>(pos1->second));
    assert(pos2 != globalP2map.end() && isa<Function>(pos2->second));

    Function *Fp1 = cast<Function>(pos1->second);
    Function *Fp2 = cast<Function>(pos2->second);

    CloneFunctionInto(Fp1, F, globalP1map, true, R1);
    CloneFunctionInto(Fp2, F, globalP2map, true, R2);
  }

  bool IFlowAnalysis::runOnFunction (Function &F)
  {
    LOG("ifsetup", errs() << "function: ");
    LOG("ifsetup", errs().write_escaped(F.getName()) << "\n");

    for (inst_iterator I = inst_begin(F), E = inst_end(F); I != E; ++I)
      LOG("ifsetup", errs() << *I << "\n");

    return false;
  }
    
  void IFlowAnalysis::getAnalysisUsage (llvm::AnalysisUsage &AU) const
  {
    AU.addRequired<seahorn::IFlowSetup>();
  } 

  void KeepaliveDeleter::visitCallSite(CallSite& CS, Instruction& I) 
  {
    const Function *cf = CS.getCalledFunction();
    if (cf && cf->getName().equals("__VERIFIER_iflow_keepalive")) {
      I.eraseFromParent();
    }
  }

  llvm::Function* IFlowAnalysis::insertVerifierError(llvm::Module& M)
  {
    // type
    std::vector<Type*> VerifierErrorTy_args;
    FunctionType* VerifierErrorTy = FunctionType::get(
        /*Result=*/Type::getVoidTy(M.getContext()),
        /*Params=*/ VerifierErrorTy_args,
        /*isVarArg=*/true);

    // declaration
    Function* func_error = M.getFunction("__VERIFIER_error");
    assert(func_error == NULL);
    func_error = Function::Create(
        /*Type=*/VerifierErrorTy,
        /*Linkage=*/GlobalValue::ExternalLinkage,
        /*Name=*/"__VERIFIER_error", &M); // (external, no body)
    func_error->setCallingConv(CallingConv::C);
    setNoUnwindAttribute(func_error, M, true /* noreturn */);
    return func_error;
  }

  llvm::Function* IFlowAnalysis::insertVerifierAssert(llvm::Module& M, llvm::Function* func_error)
  {
    // type of __VERIFIER_assert.
    std::vector<Type*>AssertTy_args;
    AssertTy_args.push_back(IntegerType::get(M.getContext(), 32));
    FunctionType* AssertTy = FunctionType::get(
        /*Result=*/Type::getVoidTy(M.getContext()),
        /*Params=*/AssertTy_args,
        /*isVarArg=*/false);

    // declaration
    Function* func_assert = M.getFunction("__VERIFIER_assert");
    assert (func_assert == NULL);
    func_assert = Function::Create(
        /*Type=*/AssertTy,
        /*Linkage=*/GlobalValue::ExternalLinkage,
        /*Name=*/"__VERIFIER_assert", &M); 
    func_assert->setCallingConv(CallingConv::C);
    setNoUnwindAttribute(func_assert, M);

    // get arg b.
    Function::arg_iterator args = func_assert->arg_begin();
    Value* int32_b = args++;
    int32_b->setName("b");

    // const 0.
    ConstantInt* const_0 = ConstantInt::get(M.getContext(), APInt(32, StringRef("0"), 10));

    // basic blocks.
    BasicBlock* label_entry = BasicBlock::Create(M.getContext(), "entry",func_assert,0);
    BasicBlock* label_if_then = BasicBlock::Create(M.getContext(), "if.then",func_assert,0);
    BasicBlock* label_if_end = BasicBlock::Create(M.getContext(), "if.end",func_assert,0);

    // code.
    // compare with zero.
    ICmpInst* int1_tobool = new ICmpInst(*label_entry, ICmpInst::ICMP_EQ, int32_b, const_0, "tobool");
    BranchInst::Create(label_if_then, label_if_end, int1_tobool, label_entry);
    // insert call if 1.
    CallInst* error_call = CallInst::Create(func_error, "", label_if_then);
    error_call->setCallingConv(CallingConv::C);
    error_call->setTailCall(true);
    setNoUnwindAttribute(error_call, M);
    new UnreachableInst(M.getContext(), label_if_then);
    // return if 0.
    ReturnInst::Create(M.getContext(), label_if_end);

    return func_assert;
  }

  void IFlowAnalysis::insertNewMain(llvm::Module& M, IFlowSetup& ifsetup)
  {
    // Type Definitions
    PointerType* PointerTy_0 = PointerType::get(IntegerType::get(M.getContext(), 32), 0);

    // type of main.
    std::vector<Type*>MainTy_args;
    FunctionType* MainTy = FunctionType::get(
        /*Result=*/IntegerType::get(M.getContext(), 32),
        /*Params=*/MainTy_args,
        /*isVarArg=*/false);

    // type of verifier assume and assert.
    std::vector<Type*>VerifyTy_args;
    VerifyTy_args.push_back(IntegerType::get(M.getContext(), 32));
    FunctionType* VerifyFnTy = FunctionType::get(
        /*Result=*/Type::getVoidTy(M.getContext()),
        /*Params=*/VerifyTy_args,
        /*isVarArg=*/false);

    // Function Declarations
    Function* func_main = M.getFunction("main");
    assert (func_main == NULL);
    func_main = Function::Create(
        /*Type=*/MainTy,
        /*Linkage=*/GlobalValue::ExternalLinkage,
        /*Name=*/"main", &M); 
    func_main->setCallingConv(CallingConv::C);
    setNoUnwindAttribute(func_main, M);

    // verifier assume and assert.
    Function* func___VERIFIER_assume = M.getFunction("__VERIFIER_assume");
    if (!func___VERIFIER_assume) {
      func___VERIFIER_assume = Function::Create(
          /*Type=*/VerifyFnTy,
          /*Linkage=*/GlobalValue::ExternalLinkage,
          /*Name=*/"__VERIFIER_assume", &M); // (external, no body)
      func___VERIFIER_assume->setCallingConv(CallingConv::C);
    }

    // get the error function.
    Function* func_error = insertVerifierError(M);
    assert(func_error != NULL);

    // and the assert function.
    Function* func_assert = insertVerifierAssert(M, func_error); 
    assert(func_assert != NULL);

    // now the two copies of main.
    Function* func_main1 = M.getFunction("main.p1");
    assert (func_main1 != NULL);

    Function* func_main2 = M.getFunction("main.p2");
    assert (func_main2 != NULL);

    // Global Variable Declarations


    // Function Definitions

    // Function: main (func_main)
    {

      BasicBlock* label_entry = BasicBlock::Create(M.getContext(), "entry",func_main,0);

      // Block entry (label_entry)
      for (auto sgi : ifsetup.secretGlobalInfo) {
        insertInit(M, label_entry, sgi);
      }

      CallInst* main_call1 = CallInst::Create(func_main1, "call1", label_entry);
      main_call1->setCallingConv(CallingConv::C);
      main_call1->setTailCall(true);
      setNoUnwindAttribute(main_call1, M);

      CallInst* main_call2 = CallInst::Create(func_main2, "call2", label_entry);
      main_call2->setCallingConv(CallingConv::C);
      main_call2->setTailCall(true);
      setNoUnwindAttribute(main_call2, M);

      ICmpInst* cmp_main_ret = new ICmpInst(*label_entry, ICmpInst::ICMP_EQ, main_call1, main_call2, "cmp_main_ret");
      CastInst* cmp_main_ret_cast = new ZExtInst(cmp_main_ret, IntegerType::get(M.getContext(), 32), "conv_main_ret", label_entry);
      CallInst* check_ret = CallInst::Create(func_assert, cmp_main_ret_cast, "", label_entry);
      check_ret->setCallingConv(CallingConv::C);
      check_ret->setTailCall(true);
      setNoUnwindAttribute(check_ret, M);

      for (auto pgi : ifsetup.publicGlobalInfo) {
        assertEquality(M, label_entry, func_assert, pgi);
      }

      ReturnInst::Create(M.getContext(), main_call1, label_entry);
    }
  }

  void IFlowAnalysis::insertInit(llvm::Module& M, llvm::BasicBlock* bb, SecretGlobalInfo& sgi)
  {
    GlobalVariable* gv1 = sgi.GV1;
    GlobalVariable* gv2 = sgi.GV2;

    assert(gv1->getType() == gv2->getType());
    PointerType* ptrType = gv1->getType();
    Type* type = ptrType->getElementType();

    PointerType* voidPtrType = PointerType::get(
            IntegerType::get(M.getContext(), 8), 
            ptrType->getAddressSpace());
    CastInst* c1 = CastInst::Create(Instruction::BitCast, gv1, voidPtrType, "", bb);
    CastInst* c2 = CastInst::Create(Instruction::BitCast, gv2, voidPtrType, "", bb);
    Value *args[] = { c1, c2 };
    CallInst* call = CallInst::Create(sgi.InitF, args, "", bb);
    //call->setCallingConv(CallingConv::C);
    //call->setTailCall(true);
    //setNoUnwindAttribute(call, M);
  }

  void IFlowAnalysis::assertEquality(llvm::Module& M, llvm::BasicBlock* bb, llvm::Function* func_assert, PublicGlobalInfo& pgi)
  {
    GlobalVariable* gv1 = pgi.GV1;
    GlobalVariable* gv2 = pgi.GV2;
    Function* EQF = pgi.EQF;

    assert(gv1->getType() == gv2->getType());
    PointerType* ptrType = gv1->getType();
    Type* type = ptrType->getElementType();

    if (pgi.EQF != NULL) {
      PointerType* voidPtrType = PointerType::get(
                                  IntegerType::get(M.getContext(), 8), 
                                  ptrType->getAddressSpace());
      CastInst* c1 = CastInst::Create(Instruction::BitCast, gv1, voidPtrType, "", bb);
      CastInst* c2 = CastInst::Create(Instruction::BitCast, gv2, voidPtrType, "", bb);
      Value *args[] = { c1, c2 };
      CallInst* cmp = CallInst::Create(pgi.EQF, args, "", bb);
      CallInst* check_call = CallInst::Create(func_assert, cmp, "", bb);
      check_call->setCallingConv(CallingConv::C);
      check_call->setTailCall(true);
      setNoUnwindAttribute(check_call, M);
    } else if (type->isIntegerTy()) {
      LoadInst* load1 = new LoadInst(gv1, "", false, bb);
      LoadInst* load2 = new LoadInst(gv2, "", false, bb);
      ICmpInst* cmp = new ICmpInst(*bb, ICmpInst::ICMP_EQ, load1, load2, "");
      CastInst* conv = new ZExtInst(cmp, IntegerType::get(M.getContext(), 32), "", bb);
      CallInst* check_call = CallInst::Create(func_assert, conv, "", bb);
      check_call->setCallingConv(CallingConv::C);
      check_call->setTailCall(true);
      setNoUnwindAttribute(check_call, M);
    } else if(type->isFloatingPointTy()) {
      LoadInst* load1 = new LoadInst(gv1, "", false, bb);
      LoadInst* load2 = new LoadInst(gv2, "", false, bb);
      FCmpInst* cmp = new FCmpInst(*bb, FCmpInst::FCMP_OEQ, load1, load2, "");
      CastInst* conv = new ZExtInst(cmp, IntegerType::get(M.getContext(), 32), "", bb);
      CallInst* check_call = CallInst::Create(func_assert, conv, "", bb);
      check_call->setCallingConv(CallingConv::C);
      check_call->setTailCall(true);
      setNoUnwindAttribute(check_call, M);
    } else {
      std::cout << "ERROR! Unknown type in assert equality." << std::endl;
      assert(false);
    }
  }

  void IFlowAnalysis::setNoUnwindAttribute(llvm::Function* func, llvm::Module& M, bool noReturn) 
  {
    AttributeSet func_PAL;
    {
      SmallVector<AttributeSet, 4> Attrs;
      AttributeSet PAS;
      {
        AttrBuilder B;
        B.addAttribute(Attribute::NoUnwind);
        if (noReturn) {
          B.addAttribute(Attribute::NoReturn);
        }
        PAS = AttributeSet::get(M.getContext(), ~0U, B);
      }

      Attrs.push_back(PAS);
      func_PAL = AttributeSet::get(M.getContext(), Attrs);

    }
    func->setAttributes(func_PAL);
  }

  void IFlowAnalysis::setNoUnwindAttribute(llvm::CallInst* callinst, llvm::Module& M)
  {
    AttributeSet call_PAL;
    {
      SmallVector<AttributeSet, 4> Attrs;
      AttributeSet PAS;
      {
        AttrBuilder B;
        B.addAttribute(Attribute::NoUnwind);
        PAS = AttributeSet::get(M.getContext(), ~0U, B);
      }

      Attrs.push_back(PAS);
      call_PAL = AttributeSet::get(M.getContext(), Attrs);

    }
    callinst->setAttributes(call_PAL);
  }
}

static llvm::RegisterPass<seahorn::IFlowAnalysis> 
X ("iflowanalysis", "Information flow analysis");
   
