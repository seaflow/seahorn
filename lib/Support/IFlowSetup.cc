
#include "seahorn/Support/IFlowSetup.hh"
#include "llvm/Transforms/Utils/UnifyFunctionExitNodes.h"
#include "llvm/Transforms/Utils/BasicBlockUtils.h"

#include <iostream>

namespace seahorn
{
  using namespace llvm;

  char IFlowSetup::ID = 0;

  bool IFlowSetup::runOnModule (llvm::Module &M)
  {
    setupGlobals(M);

    return true;
  }

  bool IFlowSetup::runOnFunction (Function &F)
  {
    return false;
  }
    

  void IFlowSetup::setupGlobals(llvm::Module& M)
  {
    // find all secret globals.
    SecureGlobalFinder SVF;
    SVF.visit(M);

    // find any specialized comparison functions.
    ComparisonFnFinder ECF(*this);
    ECF.visit(M);

    // now distinguish between secret and public globals.
    for (Module::global_iterator G = M.global_begin(); G != M.global_end(); G++) {
      GlobalVariable &GV = *G;
      auto pos = SVF.secretGlobals.find(&GV);
      if (pos != SVF.secretGlobals.end()) {
          Function* initFn = pos->second;
          secretGlobals.push_back(&GV);
          SecretGlobalInfo SGI = { &GV, NULL, NULL, initFn };
          secretGlobalInfo.push_back(SGI);

          LOG("ifsetup", errs() << "SECRET: ");
          LOG("ifsetup", errs().write_escaped(GV.getName()) << "\n");
      } else {
          publicGlobals.push_back(&GV);

          LOG("ifsetup", errs() << "PUBLIC: ");
          LOG("ifsetup", errs().write_escaped(GV.getName()) << "\n");
      }
    }
  }

  // boilerplate.
  void IFlowSetup::getAnalysisUsage (llvm::AnalysisUsage &AU) const
  {
  } 

  void SecureGlobalFinder::visitCallSite(CallSite& CS, Instruction& I) 
  {
    const Function *cf = CS.getCalledFunction();
    if (cf && cf->getName().equals("__VERIFIER_iflow_secret")) {
      LOG("ifsetup", errs() << "call: " << I << "\n");

      // FIXME:spramod check arguments and types of the function call.
      Value* arg0 = CS.getArgument(0)->stripPointerCasts();
      assert (isa<GlobalVariable>(arg0));
      GlobalVariable *gvar = cast<GlobalVariable>(arg0);
      assert (gvar != NULL);

      Value* arg1 = CS.getArgument(1)->stripPointerCasts();
      assert (isa<Function>(arg1));
      Function* initfn = cast<Function>(arg1);
      secretGlobals[gvar] = initfn;

      LOG("ifsetup", errs() << "global: " << gvar->getName().str() 
                            << "; init: " << initfn->getName().str() 
                            << "\n");
      I.eraseFromParent();
    }
  }

  void ComparisonFnFinder::visitCallSite(CallSite& CS, Instruction& I)
  {
    const Function *cf = CS.getCalledFunction();
    if (cf && cf->getName().equals("__VERIFIER_iflow_cmpfun")) {
      LOG("ifsetup", errs() << "call: " << I << "\n");

      // FIXME:spramod check arguments and types of the function call.
      Value* arg0 = CS.getArgument(0)->stripPointerCasts();
      assert (isa<GlobalVariable>(arg0));
      GlobalVariable *gvar = cast<GlobalVariable>(arg0);
      assert (gvar != NULL);

      Value* arg1 = CS.getArgument(1)->stripPointerCasts();
      assert (isa<Function>(arg1));
      Function *cmpfn = cast<Function>(arg1);

      LOG("ifsetup", errs() << "global variable: " << gvar->getName().str() 
                            << "; cmp_fn: " << cmpfn->getName().str() << "\n");

      parent.eqCmpFnMap[gvar] = cmpfn;
      parent.noCopyFns.insert(cmpfn);

      I.eraseFromParent();
    }
  }
}




