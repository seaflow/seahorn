extern int h;
extern int l;

extern int  __VERIFIER_nondet_int();
extern void __VERIFIER_iflow_secret(void *h, void (*initfn)(void*, void*));

void init_secrets(void* v_h1, void* v_h2)
{
    int *h1 = (int*) v_h1;
    int *h2 = (int*) v_h2;
    *h1 = __VERIFIER_nondet_int();
    *h2 = __VERIFIER_nondet_int();
}

int main() {
    __VERIFIER_iflow_secret(&h, init_secrets);
    l = 0;

    if (h==1) {
        l = 10;
    }
    return 0;
}
