float h;
float l;

extern float __VERIFIER_nondet_float();
extern void __VERIFIER_iflow_secret(void *h, void (*initfn)(void*, void*));

void init_secrets(void* v_h1, void* v_h2)
{
    float *h1 = (int*) v_h1;
    float *h2 = (int*) v_h2;
    *h1 = __VERIFIER_nondet_float();
    *h2 = __VERIFIER_nondet_float();
}

float foo(float x) {
  if (x != 1.0f) {
    return 10.0f;
  } else {
    return 20.0f;
  }
}

int main() {
    __VERIFIER_iflow_secret(&h, init_secrets);
    l = 0;

    if (h==1) {
      l = 10.0f;
    } else {
      l = foo(h);
    }

    return 0;
}
