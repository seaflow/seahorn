
int l;
extern void __VERIFIER_iflow_keepalive(void *var);

int fib(int n)
{
  if(n<=1) return 1;
  int a = 1;
  int b = 1;
  for (int i=2; i <= n; i++) {
    int c = a+b;
    a = b;
    b = c;
  }
  return b;
}
int main() {
    l = fib(80);
    __VERIFIER_iflow_keepalive(&l);
    return 0;
}
