#! /usr/bin/python2.7

from runparam import arrayprog
from list1 import code2

def main():
    name = 'array1'
    with open('output/array1.out', 'wt') as fout:
        for n in [10, 20, 40, 80, 160, 320, 640, 1280]:
            for verif in ['cbmc', 'seabmc', 'seahorn']:
                outfile = 'output/list%d_%s.out' % (n, verif)
                out, res, tdiff = arrayprog(name, code2, n, verif, 1800, outfile)
                print >> fout, '[%40s] %10s %6.2f' % (out, res, tdiff)

if __name__ == '__main__':
    main()
