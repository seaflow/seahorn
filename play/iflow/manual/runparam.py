
import argparse
import subprocess
import time

def getcmd(v, f, time_limit, bnd):
    if v == 'cbmc':
        return 'timeout %d cbmc --unwind %d %s' % (time_limit, bnd, f)
    elif v == 'seahorn':
        return 'timeout %d sea pf --crab --inline --cpu-limit=%d %s' % (time_limit, time_limit+30, time_limit, f)
    elif v == 'seabmc':
        return 'timeout %d sea bpf --bound=%d --crab --inline --cpu-limit=%d %s' % (time_limit, bnd, time_limit+30, f)
    else:
        assert False

def arrayprog(name, code2, N, verifier, time_limit, outfile):
    bnd = 2*N + 10
    code1 = '#define N %d\n' % N
    code = code1 + code2
    filename = '/tmp/%s_%d.c' % (name, N)
    with open(filename, 'wt') as f:
        print >> f, code

    cmd = getcmd(verifier, filename, time_limit, bnd)
    print cmd
    timeout = False
    start = time.time()
    proc = subprocess.Popen(cmd, shell=True, 
                            stdin=subprocess.PIPE, 
                            stdout=subprocess.PIPE, 
                            stderr=subprocess.STDOUT, 
                            bufsize=-1)
    output, err = proc.communicate()
    if outfile != '':
        with open(outfile, 'wt') as fout:
            print >> fout, cmd
            print >> fout, output
    else:
        print cmd
        print output
    lines = [l.strip() for l in output.split('\n') if len(l.strip())]
    if proc.returncode == 124 or (len(lines) and lines[-1] == 'timeout'):
        timeout = True
    end = time.time()
    tdiff = end-start

    result = 'finished' if not timeout else 'timeout'
    out = lines[-2] + '; ' + lines[-1] if len(lines) > 1 else lines[-1]
    print '[%50s] %10s %6.2f' % (out, result, tdiff)
    return out, result, tdiff

def main(name, code2):
    parser = argparse.ArgumentParser()
    parser.add_argument('N', help='value of N', type=int, default=5)
    parser.add_argument('verifier', help='verifier (cbmc/seahorn/seabmc)', 
                        type=str, default='seahorn', 
                        choices=['cbmc', 'seahorn', 'seabmc'])
    parser.add_argument('--timelimit', help='verifier timelimit in seconds (default=1800)', 
                        type=int, default='1800')
    parser.add_argument('--output', help='verifier output file', 
                        default='', type=str)
    args = parser.parse_args()
    arrayprog(name, code2, args.N, args.verifier, args.timelimit, args.output)

