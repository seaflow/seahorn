#define nd __VERIFIER_nondet_int
#define ndc __VERIFIER_nondet_char
#define assume __VERIFIER_assume

extern int  __VERIFIER_nondet_int();
extern int  __VERIFIER_nondet_char();
extern void __VERIFIER_assume (int);
extern void __VERIFIER_error (void);

void assert(int b)
{
    if (!b) {
        __VERIFIER_error();
    }
}

int strcmp(const char* s1, const char* s2)
{
    int i=0;
    for (; s1[i] != '\0' && s2[i] != '\0'; i++) {
        if (s1[i] != s2[i]) return 0;
    }
    if (s1[i] != '\0' || s2[i] != '\0') {
        return 0;
    }
    return 1;
}

int fib(int n)
{
  if(n<=1) return 1;
  int a = 1;
  int b = 1;
  for (int i=2; i <= n; i++) {
    int c = a+b;
    a = b;
    b = c;
  }
  return b;
}

int main() 
{
    char pwd[20] = {
        ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), 
        ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), '\0' };

    char inp1[20] = {
        ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), 
        ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), '\0' };

    char inp2[20] = {
        ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), 
        ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), ndc(), '\0' };

    int h1 = nd(), h2 = nd();
    int c = nd();
    int l1 = c + h1, l2 = c + h2;

    int cmp1 = strcmp(inp1, pwd) == 0;
    if (cmp1) {
        l1 = l1 % 16;
    } else {
        l1 = l1 - h1;
        l1 = l1 + fib(l1);
    }

    int cmp2 = strcmp(inp1, pwd) == 0;
    if (cmp2) {
        l2 = l2 % 16;
    } else {
        l2 = l2 - h2;
        l2 = l2 + fib(l2);
    }

    assert (cmp1 || cmp2 || l1 == l2);
    return 0;
}

