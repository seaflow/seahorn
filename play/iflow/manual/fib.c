#define nd __VERIFIER_nondet_int
#define assume __VERIFIER_assume

extern int  __VERIFIER_nondet_int();
extern void __VERIFIER_assume (int);
extern void __VERIFIER_error (void);

void assert(int b)
{
    if (!b) {
        __VERIFIER_error();
    }
}


int fib(int n)
{
  if(n<=1) return 1;
  int a = 1;
  int b = 1;
  for (int i=2; i <= n; i++) {
    int c = a+b;
    a = b;
    b = c;
  }
  return b;
}

int main()
{
    int n = nd();
    int f1 = fib(n);
    int f2 = fib(n);

    assert(f1 == f2);

    return 0;
}
