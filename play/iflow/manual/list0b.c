#define N 3

#include <stdlib.h>

#define nd __VERIFIER_nondet_int
#define assume __VERIFIER_assume

extern int  __VERIFIER_nondet_int();
extern void __VERIFIER_assume (int);
extern void __VERIFIER_error (void);

void assert(int b)
{
    if (!b) {
        __VERIFIER_error();
    }
}

struct node
{
    int data;
    int secret;
    struct node* next;
};

struct node* init_list(int data, int secret)
{
    struct node* n = (struct node*) malloc(sizeof(struct node));
    assume(n != NULL);
    if (n == NULL) return NULL;
    n->data = data;
    n->secret = secret;
    n->next = NULL;
    return n;
}

struct node* add_to_list(struct node* head, int data, int secret)
{
    struct node *node, *ptr;
    node = init_list(data, secret);
    if (node) {
        node->next = head;
        return node;
    } else {
        return head;
    }
}

int nondets[2*N];

void init_nd()
{
    int i;
    for(i=0; i < 2*N; i++) { nondets[i] = nd(); }
}

int get_nd(int i)
{
    assert (i < 2*N);
    return nondets[i];
}

struct node* init_data()
{
    int sec = nd();
    struct node *h1 = NULL, *h2 = NULL, *h3 = NULL, *hp = NULL;
    int secd, datd;
    int i;

    for(i=0; i < N; i++) {
        secd = get_nd(2*i);
        datd = secd ? (sec+i) : get_nd(2*i+1); 
        h3 = init_list(datd, secd);
        h3->next = hp;
        hp = h3;
    }

    /*
    i=1;
    secd = get_nd(2*i);
    datd = secd ? (sec+i) : get_nd(2*i+1); 
    h3 = init_list(datd, secd);
    h3->next = hp;
    hp = h3;

    i=2;
    secd = get_nd(2*i);
    datd = secd ? (sec+i) : get_nd(2*i+1); 
    h3 = init_list(datd, secd);
    h3->next = hp;
    hp = h3;
    */

    return h3;
}

int sum_public(struct node* head)
{
    int sum = 0;

    while (head != NULL) {
        sum += (!head->secret) ? head->data : 0;
        head = head->next;
    }

    assert(head == NULL);

    return sum;
}

int main() {
    struct node *h1, *h2;
    int s1, s2; 

    init_nd();
    h1 = init_data();
    h2 = init_data();
    s1 = sum_public(h1);
    s2 = sum_public(h2);
    assert(s1 == s2);
    return 0;
}
