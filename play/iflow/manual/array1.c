
extern int  __VERIFIER_nondet_int();
extern void __VERIFIER_assume (int);
extern void __VERIFIER_error (void);

#define N 7
#define nd __VERIFIER_nondet_int

int secret1, secret2;
int array1[N], array2[N];
int flags1[N], flags2[N];

int nondets[2*N];

void init_nd()
{
    int i;
    for(i=0; i < 2*N; i++) { nondets[i] = nd(); }
}

int get_nd(int i)
{
    if (i >= 2*N) {
        __VERIFIER_error();
    }
    return nondets[i];
}

void init_data(int* sec, int* arr, int *f)
{
    int i;
    *sec = nd();
    for(i=0; i<N; i++) { 
        if (get_nd(2*i)) { 
            f[i] = 1;
            arr[i] = get_nd(2*i+1); 
        } else {
            f[i] = 0;
            arr[i] = *sec+i;
        }
    }
}

int sum(int* f, int *arr, int p)
{
    int i, sum = 0;
    for(i=0; i < N; i++) {
        if(f[i] == p) {
            sum += arr[i];
        }
    }
    return sum;
}

int main() {
    int s1, s2;
    init_nd();
    init_data(&secret1, array1, flags1);
    init_data(&secret2, array2, flags2);
    s1 = sum(flags1, array1, 1);
    s2 = sum(flags2, array2, 1);
    if (s1 != s2) {
        __VERIFIER_error();
    }
    return 0;
}
