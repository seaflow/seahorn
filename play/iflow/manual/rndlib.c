#include <stdio.h>
#include <stdlib.h>

int  __VERIFIER_nondet_int()
{
    return rand();
}

void __VERIFIER_assume (int b)
{
    if (!b) {
        printf("ASSUMPTION failed!\n");
        exit(1);
    }
}

void __VERIFIER_error (void)
{
    printf("ERROR\n");
    exit(1);
}


