#! /bin/bash

rm times.out
for i in {1..10};
do
    n=${i}0
    echo $n >> times.out
    ./array1.py $n cbmc --timelimit 1800 --output array1_cbmc_t$n.out >> times.out
    ./array1.py $n seahorn --timelimit 1800 --output array1_seahorn_t$n.out >> times.out
done
