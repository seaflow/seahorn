#define N 1

#include <stdlib.h>

#define nd __VERIFIER_nondet_int
#define assume __VERIFIER_assume

extern int  __VERIFIER_nondet_int();
extern void __VERIFIER_assume (int);
extern void __VERIFIER_error (void);

void assert(int b)
{
    if (!b) {
        __VERIFIER_error();
    }
}

struct node
{
    int data;
    int secret;
    struct node* next;
};

struct node* init_list(int data, int secret)
{
    struct node* n = (struct node*) malloc(sizeof(struct node));
    assume (n != NULL);
    if (n == NULL) { return NULL; }
    n->data = data;
    n->secret = secret;
    n->next = NULL;
    return n;
}

int sum_public(struct node* head)
{
    int sum = 0;
    struct node *ptr = head;
    while (ptr != NULL) {
        if (!ptr->secret) {
            sum += ptr->data;
        }
        ptr = ptr->next;
    }
    return sum;
}

int main() {
    struct node *h1, *h2;
    int s1, s2;

    int n1 = nd();
    int n2 = nd();

    h1 = init_list(n1, 1);
    h2 = init_list(n2, 1);


    s1 = sum_public(h1);
    s2 = sum_public(h2);

    assert(s1 == s2);
    return 0;
}
