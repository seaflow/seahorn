int h;
int l;

extern int __VERIFIER_nondet_int();
extern void __VERIFIER_iflow_secret(void *h, void (*initfn)(void*, void*));
extern void __VERIFIER_iflow_keepalive(void *var);
extern void __VERIFIER_iflow_cmpfun(void *var, int (*cmpfn) (void*, void*));

void init_secrets(void* v_h1, void* v_h2)
{
    int *h1 = (int*) v_h1;
    int *h2 = (int*) v_h2;
    *h1 = __VERIFIER_nondet_int();
    *h2 = __VERIFIER_nondet_int();
}

void foo(int *a, int *b) {
    int z = 2;
    int y = 10;
    int x = 0;

    if (*a) x = 1;
    if (!*a) x = z;
    *b = x + y;
}

int cmpfn(void* a, void* b)
{
  int *ai = (int*)a;
  int *bi = (int*)b;
  return (*ai - *bi) == 0 || (*ai - *bi == 1) || (*ai - *bi == -1);
}

int main()
{
    __VERIFIER_iflow_secret(&h, init_secrets);

    foo(&h, &l);
    __VERIFIER_iflow_keepalive(&l);
    __VERIFIER_iflow_cmpfun(&l, cmpfn);

    return 0;
}
