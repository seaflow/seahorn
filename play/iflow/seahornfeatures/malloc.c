#include <stdlib.h>

extern int __VERIFIER_nondet_int();
extern void __VERIFIER_error (void) __attribute__((noreturn)) ;

void check(int b)
{
    if (!b) {
        __VERIFIER_error();
    }
}

int main()
{
  int *arr = malloc(sizeof(int)*4);
  if (arr == NULL) return 1;

  arr[0] = __VERIFIER_nondet_int();
  arr[1] = __VERIFIER_nondet_int();
  if (!(arr[0] > 0 && arr[0] < 10)) return 1;
  if (!(arr[1] > 0 && arr[1] < 10)) return 1;

  arr[2] = arr[0] + arr[1];
  check(arr[2] > 1 && arr[2] < 19);

  arr[3] = arr[2] + arr[1]; 
  check(arr[3] == arr[0] + 2*arr[1]);

  free(arr);
  return 0;
}
