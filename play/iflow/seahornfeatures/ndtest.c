
extern void __VERIFIER_error (void) __attribute__((noreturn)) ;

void* dosomething();

int main() {
  int* ptr = (int*) dosomething();
  if (ptr[3] == 120) {
    __VERIFIER_error();
  }
  return 0;
}

