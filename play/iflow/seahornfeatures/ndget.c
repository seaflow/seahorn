#include <stdlib.h>

extern void __VERIFIER_error (void) __attribute__((noreturn)) ;

void check(int b)
{
    if (!b) {
        __VERIFIER_error();
    }
}

int ndget(int tid)
{
  static const int DEFAULTSIZE = 16;
  static int bufsiz = 0;
  static int rndsiz = 0;
  static int pos0 = 0, pos1 = 0;
  static int *buf = NULL;

  if (bufsiz == 0) {
    check (buf == NULL);
    buf = malloc(DEFAULTSIZE*sizeof(int));
    bufsiz = DEFAULTSIZE;
  }

  check (buf != NULL);
  check (bufsiz > 0);
  check (rndsiz <= bufsiz);

  int* poscur = tid == 0 ? &pos0 : &pos1;
  if (*poscur >= rndsiz) {
    if (rndsiz == bufsiz) {
      int *bufc = malloc(sizeof(int)*bufsiz*2);
      if (bufc == NULL) exit(1);
      for(int i=0; i < bufsiz; i++) {
        bufc[i] = buf[i];
      }
      free(buf);
      buf = bufc;
      bufsiz = 2*bufsiz;
    } 
    check (rndsiz < bufsiz);
    // stuff a new nd value into the buffer.
    buf[rndsiz] = nondet_int();
    rndsiz += 1;
  }
  check(rndsiz <= bufsiz);
  check(*poscur < rndsiz);

  int nd = buf[*poscur];
  (*poscur)++;
  return nd;
}

int main()
{
  static const int SZ=64;
  int arr1[SZ], arr2[SZ];
  int t0c = 0, t1c = 0;

  while(t0c < SZ || t1c < SZ) {
    if (nondet_int()) {
      if(t0c < SZ) { arr1[t0c++] = ndget(0); } 
      else if(t1c < SZ) { arr2[t1c++] = ndget(1); } 
    } else {
      if(t1c < SZ) { arr2[t1c++] = ndget(1); } 
      else if(t0c < SZ) { arr1[t0c++] = ndget(0); }
    } 
  }

  check (t0c == SZ);
  check (t1c == SZ);


  for(int i=0; i < SZ; i++) {
    check(arr1[i] == arr2[i]);
  }

  return 0;
}

