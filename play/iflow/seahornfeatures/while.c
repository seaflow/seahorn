extern void __VERIFIER_error (void) __attribute__((noreturn)) ;
extern int __VERIFIER_nondet_int();

int main() {
  int n = __VERIFIER_nondet_int();
  int np = n;
  int c;

  for (c=1; c != 0; ) {
    if (n > 100) {
      n = n - 10;
      c--;
    } else {
      n = n + 11;
      c++;
    }
  }
  if (np <= 100 && n != 91) {
    __VERIFIER_error();
  }
  if (np > 100 && n != np - 10) {
    __VERIFIER_error();
  }
  return 0;
}

