#include <stdlib.h>

extern int __VERIFIER_nondet_int();
extern void __VERIFIER_error (void) __attribute__((noreturn)) ;

void check(int b)
{
    if (!b) {
        __VERIFIER_error();
    }
}

int main()
{
  int a = __VERIFIER_nondet_int();
  int b = __VERIFIER_nondet_int();
  if (!(a > 0 && a < 10)) return 1;
  if (!(b > 0 && b < 10)) return 1;

  int c = a + b;
  check(c > 1 && c < 19);

  int d = c + b; // a + b + n
  check(d == a + 2*b);

  int *arr = malloc(sizeof(int)*4);
  if (arr == NULL) return 1;

  arr[0] = a;
  arr[1] = b;
  arr[2] = c;
  arr[3] = d;
  check(arr[3] == arr[0] + 2*arr[1]);

  int *brr = realloc(arr, sizeof(int)*5);
  if (brr == NULL) return 1;

  brr[4] = brr[3] + brr[2];
  check (brr[0] == a);
  check (brr[1] == b);
  check (brr[2] == brr[0] + brr[1]);
  check (brr[3] == brr[0] + 2*brr[1]);
  check (brr[4] == 2*brr[0] + 3*brr[1]);

  free(brr);
  return 0;
}
