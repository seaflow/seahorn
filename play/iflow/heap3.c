#define NULL ((void*)0x0)

struct node_t {
  int data;
  struct node_t* next;
};

struct node_t* hi;
struct node_t* lo;

extern int __VERIFIER_nondet_int();
extern int __VERIFIER_assume(int b);
extern void __VERIFIER_iflow_secret(void *h, void (*initfn)(void*, void*));
extern void __VERIFIER_iflow_keepalive(void *var);
extern void __VERIFIER_iflow_cmpfun(void *var, int (*cmpfn) (void*, void*));

void init_secrets(void* v_h1, void* v_h2)
{
    struct node_t *hi1 = (struct node_t*) v_h1;
    struct node_t *hi2 = (struct node_t*) v_h2;
    hi1 = (struct node_t*) malloc(sizeof(struct node_t));
    hi1->data = __VERIFIER_nondet_int();
    hi1->next = NULL;

    hi2 = (struct node_t*) malloc(sizeof(struct node_t));
    hi2->data = __VERIFIER_nondet_int();
    hi2->next = NULL;
    __VERIFIER_assume(hi1 != NULL);
    __VERIFIER_assume(hi2 != NULL);
}

int locmp(void* lo1, void*lo2)
{
  struct node_t* l1 = *((struct node_t**) lo1);
  struct node_t* l2 = *((struct node_t**) lo2);

  // if both are null, we are done.
  while (l1 != NULL && l2 != NULL) {
    if (l1->data != l2->data) return 0;
    l1 = l1->next;
    l2 = l2->next;
  }
  // both NULL means equal.
  if (l1 == NULL & l2 == NULL) return 1;
  // if we got here, at least one is NULL but not both
  else return 0;
}

int main()
{
  // declare h to be a high variable.
  __VERIFIER_iflow_secret(&hi, init_secrets);
  __VERIFIER_iflow_cmpfun(&lo, locmp);

  free(hi);

  // alloc hi and lo.
  lo = (struct node_t*) malloc(sizeof(struct node_t));
  __VERIFIER_assume(lo != NULL);

  // stuff some data into hi.
  // and now lo.
  lo->data = 10;
  lo->next = NULL;

  __VERIFIER_iflow_keepalive(lo);
}
