
int h;
int l;

extern int __VERIFIER_nondet_int();
extern void __VERIFIER_iflow_secret(void *h, void (*initfn)(void*, void*));
extern void __VERIFIER_iflow_keepalive(void *var);

void init_secrets(void* v_h1, void* v_h2)
{
    int *h1 = (int*) v_h1;
    int *h2 = (int*) v_h2;
    *h1 = __VERIFIER_nondet_int();
    *h2 = __VERIFIER_nondet_int();
}

int main() {
    int z = 1;
    int x = 0, y=10;

    __VERIFIER_iflow_secret(&h, init_secrets);

    if (h) x = 1;
    if (!h) x = z;
    l = x + y;
    __VERIFIER_iflow_keepalive(&l);
    return 0;
}
