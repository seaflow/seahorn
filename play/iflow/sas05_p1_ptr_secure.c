int h;
int l;

extern int __VERIFIER_nondet_int();
extern void __VERIFIER_iflow_secret(void *h, void (*initfn)(void*, void*));
extern void __VERIFIER_iflow_keepalive(void *var);

void init_secrets(void* v_h1, void* v_h2)
{
    int *h1 = (int*) v_h1;
    int *h2 = (int*) v_h2;
    *h1 = __VERIFIER_nondet_int();
    *h2 = __VERIFIER_nondet_int();
}

void foo(int *a, int *b) {
    int z = 1;
    int y = 10;
    int x = 0;

    if (*a) x = 1;
    if (!*a) x = z;
    *b = x + y;
}

int main()
{
    __VERIFIER_iflow_secret(&h, init_secrets);

    foo(&h, &l);
    __VERIFIER_iflow_keepalive(&l);

    return 0;
}
