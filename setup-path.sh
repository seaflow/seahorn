#! /bin/bash

if ! [[ $(type -P sea) ]] ; then
    echo "sea not in PATH"
    export PATH=$PATH:$PWD/build/run/bin/
else
    echo "sea already in PATH!"
    which sea
fi
