# SeaHorn #
[![Build Status](https://travis-ci.org/seahorn/seahorn.svg?branch=master)](https://travis-ci.org/seahorn/seahorn)

![18093415-vector-illustration-of-seahorse-cartoon--coloring-book.jpg](https://bitbucket.org/repo/gngGo9/images/174701276-18093415-vector-illustration-of-seahorse-cartoon--coloring-book.jpg)

#About#

An LLVM based verification framework.

#License#
SeaHorn is distributed under a modified BSD license. See [license.txt](license.txt) for details.

#Compilation#

* `cd seahorn ; mkdir build ; cd build`
* `cmake -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX=run ../ `
* (optional) `cmake --build . --target extra` to download extra packages
* `cmake --build .` to build dependencies (Z3 and LLVM)
* (optional) `cmake --build .` to build extra packages (crab-llvm)
* `cmake --build .` to build seahorn
* `cmake --build . --target install` to install everything in `run` directory

SeaHorn and dependencies are installed in `build/run`

Optional components can be installed individually as well:

* [dsa-seahorn](https://bitbucket.org/seaflow/dsa-seahorn): ``` git clone https://bitbucket.org/seaflow/dsa-seahorn.git ```

* [crab-llvm](https://bitbucket.org/seaflow/crab-llvm): ``` git clone https://bitbucket.org/seaflow/crab-llvm.git```

* [llvm-seahorn](https://bitbucket.org/seaflow/llvm-seahorn): ``` git clone https://bitbucket.org/seaflow/llvm-seahorn.git```

Note that both [dsa-seahorn](https://bitbucket.org/seaflow/dsa-seahorn)
and [crab-llvm](https://bitbucket.org/seaflow/crab-llvm) are
optional. Nevertheless both are highly recommended. The former is
needed when reasoning about memory contents while the latter provides
inductive invariants using abstract interpretation techniques to the
rest of SeaHorn's backends.


#Usage#

SeaHorn provides a python script called `sea` to interact with
users. Given a C program annotated with assertions, users just need to
type: `sea pf file.c`

This will output `unsat` if all assertions hold or otherwise `sat` if
any of the assertions is violated. The option `pf` tells SeaHorn to
translate `file.c` into LLVM bitecode, generate a set of verification
conditions (VCs), and finally, solve them. This command uses as main
default options:

- `--step=large`: large-step encoding. Each step corresponds to a
loop-free program block.

- `--step=small`: small-step encoding. Each step corresponds to a
  basic block.

- `--track=mem`: model both scalars, pointers, and memory contents

- `--track=ptr` : model registers and pointers (but not memory content)

- `--track=reg` : model registers only

- `--inline` : inlines the program before verification

- `--cex=FILE` : stores a counter-example in `FILE`

- `--crab` : generates invariants using the Crab
  abstract-interpretation-based tool. Read
  [here](https://bitbucket.org/seaflow/crab-llvm) for
  details about Crab options. 

- `-g` : compiles with debug information for more trackable
  counterexamples.

`sea pf` is a pipeline that runs multiple commands. Individual parts
of the pipeline can be ran separately as well:

1. `sea fe file.c -o file.bc`: SeaHorn frontend translates a C program
  into optimized LLVM bitcode including mixed-semantics
  transformation.

2. `sea horn file.bc -o file.smt2`: SeaHorn generates the verification
  conditions from `file.bc` and outputs them into SMT-LIB v2 format. Users
  can choose between different encoding styles with several levels of
  precision by adding:

   - `--step={small,large,fsmall,flarge}` where `small` is small step
      encoding, `large` is block-large encoding, `fsmall` is small
      step encoding producing flat Horn clauses (i.e., it generates a
      transition system with only one predicate), and `flarge`:
      block-large encoding producing flat Horn clauses.

   - `--track={reg,ptr,mem}` where `reg` only models integer
      scalars, `ptr` models `reg` and pointer addresses, and `mem`
      models `ptr` and memory contents.

3. `sea smt file.c -o file.smt2`: Generates CHC in SMT-LIB2 format. Is
   an alias for `sea fe` followed by `sea horn`. The command `sea pf`
   is an alias for `sea smt --prove`.

4.  `sea clp file.c -o file.clp`: Generates CHC in CLP format.

5. `sea lfe file.c -o file.ll` : runs the legacy front-end

To see all the options, type `sea --help`.

##Annotating C programs##

This is an example of a C program annotated with a safety property:
``` c
    extern int nd();
    extern void __VERIFIER_error() __attribute__((noreturn));
    void assert (int cond) { if (!cond) __VERIFIER_error (); }
    int main(){
      int x,y;
      x=1; y=0;
      while (nd ())
      {
        x=x+y;
        y++;
      }
      assert (x>=y);
     return 0;
    }
```
SeaHorn follows [SV-COMP][svcomp] convention of encoding error locations by a call
to the designated error function 
`__VERIFIER_error()`. SeaHorn returns `unsat` when `__VERIFIER_error()`
is unreachable, and the program is considered safe. SeaHorn returns `sat`
when `__VERIFIER_error()` is reachable and the
program is unsafe.

[svcomp]: (http://sv-comp.sosy-lab.org)

#Information Flow Extensions#

Examples for information flow analysis are in the play/iflow/* folder.

The following program (play/iflow/sas05p1_secure.c) shows some of the 
annotations supported by the information flow extensions.


``` c
    int h;
    int l;

    extern int __VERIFIER_nondet_int();
    extern void __VERIFIER_iflow_secret(void *h, void (*initfn)(void*, void*));
    extern void __VERIFIER_iflow_keepalive(void *var);

    void init_secrets(void* v_h1, void* v_h2)
    {
        int *h1 = (int*) v_h1;
        int *h2 = (int*) v_h2;
        *h1 = __VERIFIER_nondet_int();
        *h2 = __VERIFIER_nondet_int();
    }

    int main() {
        int z = 1;
        int x = 0, y=10;

        __VERIFIER_iflow_secret(&h, init_secrets);

        if (h) x = 1;
        if (!h) x = z;
        l = x + y;
        __VERIFIER_iflow_keepalive(&l);
        return 0;
    }
```

The function `__VERIFIER_iflow_secret` informs the verifier that
a particular global variable is to be secret, and therefore,
should not influence any of non-secret global variables at the
end of program execution. It accepts two arguments: the first
is a pointer to the global variable, the second is a 
function which is initalizes this variable at the start of execution.

The function `__VERIFIER_iflow_keepalive` is used to ensure that
its argument is not optimized away by the compiler.

To invoke the information flow verifier on this program use

    $ sea pif play/iflow/sas05p1_secure.c

The switch `pif` to the the `sea` script invokes the information flow
analysis. A `sat` result means that information flow is possible
from a secret to a public variable, while an unsat results means that
it is not possible.


##Implementation of the Information Flow Analysis##

The information flow analysis is implemented in two passes. 

The first is called `IFlowSetup` and it is implemented in the files
`include/seahorn/Support/IFlowSetup.hh` and `lib/Support/IFlowSetup.cc`.  This
pass parses the annotations supported by the analysis and remove them from the
program.

The second pass, called `IFlowAnalysis` performs the self-composition
construction. It is implemented in the files
`include/seahorn/Transforms/Instrumentation/IFlowAnalysis.hh` and
`lib/Transforms/Instrumentation/IFlowAnalysis.cc`.

The passes are invoked from `tools/seahorn/seaifpp.cc`.

#People#

* [Arie Gurfinkel](arieg.bitbucket.org)
* [Jorge Navas](http://ti.arc.nasa.gov/profile/jorge/)
* [Temesghen Kahsai](http://www.lememta.info/)


